#pragma once
#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\ZorkishWorld\ZorkishWorld.h"
#include "..\ZorkishWorld\Location.cpp"
#include "..\ZorkishWorld\Path.cpp"
#include "..\ZorkishWorld\GameObject.cpp"
#include "..\ZorkishWorld\MessageQueue.cpp"
#include "..\ZorkishWorld\Monster.cpp"
#include "..\ZorkishWorld\Message.cpp"
#include "..\ZorkishWorld\CharacteristicComponent.cpp"
#include "..\ZorkishWorld\Destructible.cpp"
#include "..\ZorkishWorld\Flammability.cpp"
#include "..\ZorkishWorld\IdentifiableObject.cpp"
#include "..\ZorkishWorld\Utilities.cpp"
#include "..\ZorkishWorld\Inventory.cpp"
#include "..\ZorkishWorld\ItemContainer.cpp"
#include "..\ZorkishWorld\Item.cpp"
#include "..\ZorkishWorld\Command.cpp"
#include "..\ZorkishWorld\CommandManager.cpp"
#include "..\ZorkishWorld\ActionCommand.cpp"
#include "..\ZorkishWorld\AttackCommand.cpp"
#include "..\ZorkishWorld\MoveCommand.cpp"
#include "..\ZorkishWorld\LookCommand.cpp"
#include "..\ZorkishWorld\TakeCommand.cpp"
#include "..\ZorkishWorld\PutCommand.cpp"
#include "..\ZorkishWorld\Player.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTesting
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(Spike7LocationHasDestination)
		{
			Location* forest = new Location("forest", "filled with grass");
			Location* desert = new Location("desert", "be careful about sandstorm");
			Path* path = new Path("northeast", "you head to northeast", forest, desert);

			forest->AddDestination(*path);
			Assert::IsTrue(forest->getPaths().back().getDestination() == desert);
		}

		TEST_METHOD(Spike7LocationHasMultiplePaths)
		{
			Location* forest = new Location("forest", "filled with grass");
			Location* desert = new Location("desert", "be careful about sandstorm");
			Path* path = new Path("northeast", "you head to northeast", forest, desert);
			Path* path2 = new Path("southeast", "you head to southeast", forest, desert);

			forest->AddDestination(*path);
			forest->AddDestination(*path2);
			Assert::IsTrue(forest->getPaths().size() == 2);
		}
		TEST_METHOD(Spike8CommandPatternProcessingLookCommand)
		{
			CommandManager* c = new CommandManager();
			Location* forest = new Location("forest", "filled with grass");
			Location* desert = new Location("desert", "be careful about sandstorm");
			Path* path = new Path("northeast", "you head to northeast", forest, desert);
			forest->AddDestination(*path);
			Player* player = new Player(*forest);

			string commandOutput = c->processCommands("look", *player);
			string expectedOutput = "forest - filled with grass\nThere is nothing in this area \nThere is exit northeast; \nMonsters: Slime; Troll; \n";
			Assert::AreEqual(commandOutput, expectedOutput);
		}

		TEST_METHOD(Spike8CommandPatternProcessingMoveCommand)
		{
			CommandManager* c = new CommandManager();
			Location* forest = new Location("forest", "filled with grass");
			Location* desert = new Location("desert", "be careful about sandstorm");
			Path* path = new Path("northeast", "you head to northeast", forest, desert);
			forest->AddDestination(*path);
			Player* player = new Player(*forest);

			string commandOutput = c->processCommands("move northeast", *player);
			string expectedOutput = "you head to northeast\n";
			Assert::AreEqual(commandOutput, expectedOutput);
		}
		TEST_METHOD(Spike9CompositePatternStorePouchInBag)
		{
			CommandManager* c = new CommandManager();
			Location* forest = new Location("forest", "filled with grass");
			Location* desert = new Location("desert", "be careful about sandstorm");
			Path* path = new Path("northeast", "you head to northeast", forest, desert);
			forest->AddDestination(*path);
			Player* player = new Player(*forest);
			ItemContainer* bag = new ItemContainer("bag", "Bag", "a fine Bag", "container");
			ItemContainer* pouch = new ItemContainer("pouch", "Pouch", "a small pouch", "container");
			player->getInventory()->AddItem(*bag);
			player->getInventory()->AddItem(*pouch);

			//Put Puch inside bag
			c->processCommands("put pouch in bag", *player);
			//check whats inside bag
			string commandOutput = c->processCommands("look in bag", *player);
			string expectedOutput = "Pouch,  \n"; // bag only contains pouch

			Assert::IsTrue(commandOutput == expectedOutput);
		}


	};
}