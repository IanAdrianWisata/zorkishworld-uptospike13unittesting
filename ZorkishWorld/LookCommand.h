#pragma once
#include "Command.h"
class LookCommand : public Command
{
public:
	LookCommand();
	~LookCommand();

	//Methods
	string Execute(Player &aPlayer, string aText);
};

