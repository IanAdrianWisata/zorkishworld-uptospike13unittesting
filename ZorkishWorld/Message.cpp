#include "Message.h"


Message::Message(GameObject &aSender, GameObject &aDestinnation, string aType, int aData)
{
	fSender = &aSender;
	fDestination = &aDestinnation;
	fType = aType;
	fData = aData;
}

Message::Message()
{
}


Message::~Message()
{
}

GameObject * Message::getDestination()
{
	return fDestination;
}

string Message::getType()
{
	return fType;
}

int Message::getData()
{
	return fData;
}
