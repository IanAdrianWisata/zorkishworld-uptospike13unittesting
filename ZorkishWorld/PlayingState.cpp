#include "PlayingState.h"
#include "StateManager.h"


PlayingState::PlayingState() 
{
	fCommandManager = new CommandManager();
	populateLocations();
	populatePaths();
	attachPathToLocations();
	fPlayer = new Player(Location());
	if (fLocations->size() > 0)
		fPlayer->Move((*fLocations)[0]);

}


PlayingState::~PlayingState()
{
}

void PlayingState::HandleInput()
{
	if (fLocations->size() == 0)
	{
		populateLocations();
		populatePaths();
		attachPathToLocations();
		fPlayer->Move((*fLocations)[0]);
	}


	string lInput;

	getline(cin, lInput);

	if (lInput == "q")
	{
		StateManager::changeState(StateManager::MENU);
	}
	if (lInput != "")
	{
		fActionMessageResult = fCommandManager->processCommands(lInput, *fPlayer);
		cin.clear();
	}
	else
		fActionMessageResult = "";
	

	
}

void PlayingState::Draw()
{
	cout << fActionMessageResult;
}

void PlayingState::Update()
{
	fPlayer->getMessageQueue().dispatchMessage();
	fPlayer->getLocation().Update();
}

void PlayingState::populateLocations()
{
	//Setting Up Locations
	fLocations = new vector<Location>;

	vector<string> lLocations;
	vector<string> lItems;
	vector<string> lLocationItems;
	vector<string> lItemID;
	bool itemFound = false;

	for (int i = 0; i < Utilities::getLocations().size(); i++)
	{
		lLocations = Utilities::splice(Utilities::getLocations()[i], ';');
		fLocations->push_back(*(new Location(lLocations[0], lLocations[1])));

		//Check if Location has Item or not
		if (lLocations.size() > 2)
		{
			lLocationItems = Utilities::splice(lLocations[2], ',');

			//For each items in the location check if the list has it
			for (int j = 0; j < lLocationItems.size(); j++)
			{
				itemFound = false;
				//Check the ItemList
				for (int h = 0; h < Utilities::getItems().size(); h++)
				{
					if (itemFound)
					{
						itemFound = false;
						break;
					}

					lItems = Utilities::splice(Utilities::getItems()[h], ';'); //get the Item
					lItemID = Utilities::splice(lItems[0], ',');
					//Check Item ID
					for (int k = 0; k < lItemID.size(); k++)
					{
						if (lLocationItems[j] == lItemID[k])
						{
							if (lItems[3] != "null") //if item not container
								(*fLocations)[i].getInventory()->AddItem(*(new ItemContainer(lItems[0], lItems[1], lItems[2], lItems[3]))); 
							else
								(*fLocations)[i].getInventory()->AddItem(*(new Item(lItems[0], lItems[1], lItems[2], lItems[3])));

							(*fLocations)[i].getInventory()->getItem(lItems[0]).addComponent(lItems[4]);
							itemFound = true;
							break;
						}
					}		
				}
			}
		}
		
	}
}

void PlayingState::populatePaths()
{
	//Setting Up Paths
	fPaths = new vector<Path>;

	vector<string> lPaths;
	for (int i = 0; i < Utilities::getPaths().size(); i++)
	{
		Location* lStartingLocation = NULL;
		Location* lDestination = NULL;

		lPaths = Utilities::splice(Utilities::getPaths()[i], ';');

		for (int j = 0; j < fLocations->size(); j++)
		{
			if ((lPaths[2] == (*fLocations)[j].getLocationName()))
			{
				lStartingLocation = &(*fLocations)[j];
			}
			if (lPaths[3] == (*fLocations)[j].getLocationName())
			{
				lDestination = &(*fLocations)[j];
			}
		}
		fPaths->push_back(*(new Path(lPaths[0], lPaths[1], lStartingLocation, lDestination)));
	}
}

void PlayingState::attachPathToLocations()
{
	// Attach Path to Location
	for (int i = 0; i < fLocations->size(); i++)
	{
		for (int j = 0; j < fPaths->size(); j++)
		{
			if (&(*fLocations)[i] == (*fPaths)[j].getStartLocation())
			{
				Location* currentLocation = &(*fLocations)[i];
				Path* currentPath = &(*fPaths)[j];
				currentLocation->AddDestination(*currentPath);
			}
		}
	}
}