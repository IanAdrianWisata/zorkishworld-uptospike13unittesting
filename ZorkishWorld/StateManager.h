#pragma once
#include "AboutState.h"
#include "GameState.h"
#include "HallOfFameState.h"
#include "HelpState.h"
#include "MenuState.h"
#include "PlayingState.h"
#include "SelectMapState.h"
#include "NewHighScoreState.h"


class StateManager
{
private:
	static AboutState* fStateAbout;
	static HallOfFameState* fStateHallOfFame;
	static HelpState* fStateHelp;
	static MenuState* fStateMenu;
	static NewHighScoreState* fStateNewHighScore;
	static PlayingState* fStatePlaying;
	static SelectMapState* fStateSelectMap;
	static GameState* fCurrentState;
	static bool isRunning;
public:
	enum State {ABOUT,HOF,HELP,MENU,NEWHIGHSCORE,PLAYING,SELECTMAP,QUIT};
	//Constructors
	StateManager();
	~StateManager();

	//Methods
	void HandleInput();
	static void changeState(State aState);
	static bool getIsRunning();
	//Properties
	GameState* getCurrentState();
	
};



