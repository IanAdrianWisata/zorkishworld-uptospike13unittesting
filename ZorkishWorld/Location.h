#pragma once
#include <vector>
#include "Inventory.h"
#include "Item.h"
#include "Path.h"
#include "Monster.h"
using namespace std;

class Path;

class Location
{
private:
	string fName;
	string fDescription;
	vector<Path>* fPaths;
	Inventory* fInventory;
	vector<Monster*> fMonsters;
public:
	
	//Constructors
	Location(string aName, string aDescription);
	Location();
	~Location();

	//Methods
	void AddDestination(Path &aPath);
	void addMonster(Monster &aMonster);
	bool hasMonster(string &aMonsterName);
	void Update();

	//Properties
	string getLocationName();
	string getLocationDescription();
	string getPathsList();
	string getMonsterList();
	vector<Path> getPaths();
	Inventory* getInventory();
	vector<Monster*> getMonsters();
	Monster* getMonster(string &aMonsterName);
};

