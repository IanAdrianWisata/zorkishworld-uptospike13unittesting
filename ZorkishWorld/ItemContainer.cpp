#include "ItemContainer.h"



ItemContainer::ItemContainer(string aId, string aName, string aDescription, string aContainer) :Item(aId, aName, aDescription, aContainer)
{
	fInventory = new Inventory();
}

ItemContainer::ItemContainer()
{
}


ItemContainer::~ItemContainer()
{

}

Inventory ItemContainer::getInventory()
{
	return *fInventory;
}

bool ItemContainer::isContainer()
{
	return true;
}
