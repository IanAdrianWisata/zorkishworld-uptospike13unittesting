#include "LookCommand.h"



LookCommand::LookCommand():Command("look,see,check,examine,scan,peek,inspect,view")
{

}


LookCommand::~LookCommand()
{
}

string LookCommand::Execute(Player & aPlayer, string aText)
{
	vector<string> lInput = Utilities::splice(aText, ' ');
	string result;
	string lMultiplePaths = "There is exit ";
	string lMultiplePlayerItems = "There is ";;
	string lMultipleLocationItems = "There is ";;

	//Check if Location contains multiple path
	if (aPlayer.getLocation().getPaths().size() > 1)
		lMultiplePaths = "There are paths heading ";
	//Check if Player inventory has more than 1 item
	if (aPlayer.getInventory()->CheckInventory() != "")
		lMultiplePlayerItems = "There are";
	//Check if Location inventory has more than 1 item
	if (aPlayer.getLocation().getInventory()->CheckInventory() != "")
		lMultipleLocationItems = "There are ";
	

	//Example: Look
	//Return location description , items in location, and available paths;
	if (lInput.size() < 3)
	{
		if (lInput.size() == 1)
		{
			result = aPlayer.getLocation().getLocationName() + " - " + aPlayer.getLocation().getLocationDescription() + "\n";
			result += lMultipleLocationItems + aPlayer.getLocation().getInventory()->getItemList() + " in this area \n";
			result += lMultiplePaths + aPlayer.getLocation().getPathsList() + "\n";
			result += "Monsters: "+ aPlayer.getLocation().getMonsterList() +"\n";
		}
		else
		{
			result = "What do you want to look at ? \n";
		}
	}

	//Example: Look in (object) / Look at (object)
	if (lInput.size() == 3)
	{
		//Look in item
		if (lInput[1] == "in")
		{
			if (aPlayer.getInventory()->HasItem(lInput[2]))
			{
				Item* lItem = (aPlayer.getInventory()->getItemptr(lInput[2]));

				if (ItemContainer* lBag = dynamic_cast<ItemContainer*>(lItem))
				{
					result = lBag->getInventory().getItemList() + " \n";
				}

				else
				{
					result = "you cannot see inside of " + lItem->getName() + "\n";
				}
			}
			else
				result = "You don't have " + lInput[2] + "\n";
		}
		if (lInput[1] == "at")
		{
			if (aPlayer.getLocation().hasMonster(lInput[2]))
			{
				stringstream convert;
				int temp;
				result = aPlayer.getLocation().getMonster(lInput[2])->getName() + "\n";
				result += aPlayer.getLocation().getMonster(lInput[2])->getDescription() + "\n";
				temp = aPlayer.getLocation().getMonster(lInput[2])->getHealthPoints();
				convert << temp;
				result += "HP: " + convert.str() + "; ";
				temp = aPlayer.getLocation().getMonster(lInput[2])->getDamage();
				convert.str("");
				convert << temp;
				result += "Damage: " + convert.str() + "\n";
			}
			else if (aPlayer.isIdentifiable(lInput[2]))
			{
				stringstream convert;
				int temp;
				temp = aPlayer.getHealthPoints();
				convert << temp;
				result = "HP:" + convert.str() + "\n";
				convert.str("");
				result += "You have " + aPlayer.getInventory()->getItemList() + "\n";
			}

			else if (aPlayer.getInventory()->HasItem(lInput[2]))
			{
				result = aPlayer.getInventory()->getItem(lInput[2]).getDescription() + "\n";
			}
			else
				result = "I can't look at " + lInput[2] + "\n";
		}
	}
	return result;
		
}
