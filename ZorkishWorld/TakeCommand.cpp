#include "TakeCommand.h"



TakeCommand::TakeCommand() :Command("take,get")
{
}


TakeCommand::~TakeCommand()
{
}

string TakeCommand::Execute(Player &aPlayer, string aText)
{
	vector<string> lInput = Utilities::splice(aText, ' ');
	Inventory* lPlayerInventory = aPlayer.getInventory();
	string result = "";

	if (lInput.size() == 1)
		return "What do you want to take? \n";
	if (lInput.size() == 2)
	{
		if (aPlayer.getLocation().getInventory()->HasItem(lInput[1]))
		{
			Item* temp = aPlayer.getLocation().getInventory()->FetchItem(lInput[1]);
			aPlayer.getInventory()->AddItem(*temp);
			result = "You stored " + lInput[1] + " in your inventory \n";
		}
		else
			result = "There is no such item in here \n";
	}
	if (lInput.size() == 4)
	{
		if (lInput[2] == "from")
		{
			if (aPlayer.getInventory()->HasItem(lInput[3]))
			{
				Item* lContainer = (lPlayerInventory->getItemptr(lInput[3]));
				if (ItemContainer* lBag = dynamic_cast<ItemContainer*>(lContainer))
				{
					if (lBag->getInventory().HasItem(lInput[1]))
					{
						Item* temp = lBag->getInventory().FetchItem(lInput[1]);
						aPlayer.getInventory()->AddItem(*temp);
						result = "You take " + lInput[1] + " from " + lInput[3] + "\n";
					}
					else
						result = lInput[3] + " contains no " + lInput[1] + "\n";
				}		
				else
					result = lInput[3] + " cannot contain an item! \n";
			}
			else
				result = "You don't have " + lInput[3] + "\n";
		}
		else
			result = "I don't know how to do that \n";
		
		
	}
	return result;
}
