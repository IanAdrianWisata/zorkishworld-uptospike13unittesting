#include "MessageQueue.h"
#include "GameObject.h"

MessageQueue::MessageQueue()
{
	fMessages = new vector<Message>();
}


MessageQueue::~MessageQueue()
{
}

void MessageQueue::addMessage(Message &aMessage)
{
	fMessages->push_back(aMessage);
}

void MessageQueue::dispatchMessage()
{
	GameObject* lDestination;
	Message lMsg;
	for (int i = 0; i < fMessages->size(); i++)
	{
		lMsg = (*fMessages)[i];


		lDestination = lMsg.getDestination();

		if (lDestination)
		{
			lDestination->onMessage(lMsg);
		}
	}
	fMessages->clear(); //clear all the message after dispatching

}
