#include "Player.h"
#include "Message.h"
#include "Monster.h"

Player::Player(Location &aLocation) : GameObject((string)"me,player",(string)"Player",(string)"a Player)")
{
	fHealthPoints = 100;
	fBaseDamage = 10;
	fInventory = new Inventory();
	fLocation = new Location();
	Move(aLocation);
}


Player::~Player()
{
}

Inventory* Player::getInventory()
{
	return fInventory;
}

void Player::onMessage(Message aMessage)
{
	if (aMessage.getType() == "damage")
	{
		fHealthPoints -= aMessage.getData();
		cout << getName() + " has taken " << aMessage.getData() << " damage \n";
		if (fHealthPoints == 0)
			cout << "GameOver \n";
	}
}

void Player::Move(Location& aLocation)
{
	fLocation = &aLocation;
}

void Player::attack(GameObject &aTarget)
{
	Message lMsg = Message(*this, aTarget,"damage", fBaseDamage);
	getMessageQueue().addMessage(lMsg);
}

Location Player::getLocation()
{
	return *fLocation;
}

int Player::getHealthPoints()
{
	return fHealthPoints;
}

int Player::getDamage()
{
	return fBaseDamage;
}
