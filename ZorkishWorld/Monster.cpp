#include "Monster.h"
#include "Message.h"


Monster::Monster(string aId, string aName, string aDescription, int aHealthPoints,int aDamage) :GameObject(aId, aName, aDescription)
{
	fAlive = true;
	fHealthPoints = aHealthPoints;
	fDamage = aDamage;
}

Monster::Monster()
{
}


Monster::~Monster()
{
}

void Monster::onMessage(Message aMessage)
{
	
	if (aMessage.getType() == "damage")
	{
		fHealthPoints -= aMessage.getData();
		cout << getName() + " has taken " << aMessage.getData() << " damage \n";	
		if (fHealthPoints == 0)
			cout << getName() + " has been defeated!" + "\n";
	}	
	Update();
		
}

void Monster::Update()
{
	if (fHealthPoints == 0)
		fAlive = false;
}

void Monster::attack(GameObject & aTarget)
{
	Message lMsg = Message(*this, aTarget, "damage", fDamage);
	getMessageQueue().addMessage(lMsg);
}

bool Monster::isAlive()
{
	return fAlive;
}

int Monster::getHealthPoints()
{
	return fHealthPoints;
}

int Monster::getDamage()
{
	return fDamage;
}

string Monster::getStatus()
{
	return "" ;
}
