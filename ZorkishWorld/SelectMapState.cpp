#include "SelectMapState.h"
#include "StateManager.h"


SelectMapState::SelectMapState()
{

}


SelectMapState::~SelectMapState()
{
}

void SelectMapState::HandleInput()
{
	int lInput;
	cin >> lInput;

	switch (lInput)
	{
	case 0:
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		StateManager::changeState(StateManager::MENU);
		break;
	case 1:
		Utilities::eraseData();
		Utilities::readInput("MountainWorldLocations.txt","MountainWorldPaths.txt");
		Utilities::loadMap("Mountain World");
		cout << "Welcome to Zorkish World: " + Utilities::getMapName() + "\n";
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		StateManager::changeState(StateManager::PLAYING);
		break;
	case 2:
		Utilities::eraseData();
		Utilities::readInput("WaterWorldLocations.txt", "WaterWorldPaths.txt");
		Utilities::loadMap("Water World");
		cout << "Welcome to Zorkish World: " + Utilities::getMapName() + "\n";
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		StateManager::changeState(StateManager::PLAYING);
		break;
	case 3:
		Utilities::eraseData();
		Utilities::readInput("BoxWorldLocations.txt", "BoxWorldPaths.txt");
		Utilities::loadMap("Box World");
		cout << "Welcome to Zorkish World: " + Utilities::getMapName() + "\n";
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		StateManager::changeState(StateManager::PLAYING);
		break;
	default :
		cout << "Wrong Input!";
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		break;
	}
}

void SelectMapState::Draw()
{
	system("cls");
	cout << "Zorkish :: Select Map \n";
	cout << "------------------------------------------------------------------------------ \n";
	cout << "Choose your Adventure \n";
	cout << "1. Mountain World \n";
	cout << "2. Water World \n";
	cout << "3. Box World \n";
	cout << "0. To go back \n";
}

void SelectMapState::Update()
{
}
