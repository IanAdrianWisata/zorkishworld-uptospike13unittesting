#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "Utilities.h"

using namespace std;


class IdentifiableObject
{
private:
	vector<string> fIdentifiers;
public:
	//Constructor
	IdentifiableObject(string &aString);
	IdentifiableObject();
	~IdentifiableObject();

	//Methods
	bool isIdentifiable(string aString);
	int getNumberOfItem();
};

