#include "MoveCommand.h"



MoveCommand::MoveCommand():Command("move,go,walk,run")
{

}


MoveCommand::~MoveCommand()
{
}

string MoveCommand::Execute(Player &aPlayer, string aText)
{
	vector<string> lInput = Utilities::splice(aText, ' ');
	int lTraversePath = aPlayer.getLocation().getPaths().size();
	Location lCurrentLocation = aPlayer.getLocation();

	string result = "";
	for (int i = 0; i < lTraversePath; i++)
	{
		if (lInput[1] == lCurrentLocation.getPaths()[i].getId())
		{
			Location* lDestination = aPlayer.getLocation().getPaths()[i].getDestination();

			result = aPlayer.getLocation().getPaths()[i].getDescription() + "\n";
			aPlayer.Move(*lDestination);
		}
	}
	return result;
}
