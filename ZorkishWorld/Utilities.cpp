#include "Utilities.h"

vector<string> Utilities::fLocations;
vector<string> Utilities::fPaths;
vector<string> Utilities::fItems;

string Utilities::fMapName;
Utilities::Utilities()
{
}


Utilities::~Utilities()
{
}

vector<string> Utilities::splice(string aString, char aDelimiter)
{
	vector<string> result;
	stringstream lStringStream(aString); //Turn string to stream
	string lToken;

	while (getline(lStringStream, lToken, aDelimiter))
		result.push_back(lToken);

	return result;
}
void Utilities::readInput(string aLocationsFile, string aFilePaths)
{
	//Read items only once
	if (fItems.size() < 1)
	{
		ifstream lItemsFile;
		lItemsFile.open("ItemList.txt");

		if (!lItemsFile.good())
			throw("Cannot open items file");
		else
			readItemsFromTextFile(lItemsFile);

		lItemsFile.close();
	}
	ifstream lLocationsFile, lPathsFile;

	//Open all Files
	lLocationsFile.open(aLocationsFile);
	lPathsFile.open(aFilePaths);

	if (!lLocationsFile.good())
		throw("Cannot open locations file");
	else
		readLocationsFromTextFile(lLocationsFile);

	if (!lPathsFile.good())
		throw("Cannot open paths file");
	else
		readPathsFromTextFile(lPathsFile);

	lLocationsFile.close();
	lPathsFile.close();
}

void Utilities::readLocationsFromTextFile(ifstream & aInput)
{
	string lText;
	while (!aInput.eof())
	{
		getline(aInput, lText);
		fLocations.push_back(lText);
	}
}

void Utilities::readPathsFromTextFile(ifstream & aInput)
{
	string lText;
	while (!aInput.eof())
	{
		getline(aInput, lText);
		fPaths.push_back(lText);
	}
}

void Utilities::readItemsFromTextFile(ifstream &aInput)
{
	string lText;
	while (!aInput.eof())
	{
		getline(aInput, lText);
		fItems.push_back(lText);
	}
}

void Utilities::loadMap(string aMapName)
{
	fMapName = aMapName;
}

void Utilities::eraseData()
{
	if (fLocations.size() != 0 || fPaths.size() != 0 )
	{
		fLocations.clear();
		fPaths.clear();
	}
}

vector<string> Utilities::getItems()
{
	return fItems;
}


vector<string> Utilities::getLocations()
{
	return fLocations;
}

vector<string> Utilities::getPaths()
{
	return fPaths;
}

string Utilities::getMapName()
{
	return fMapName;
}
