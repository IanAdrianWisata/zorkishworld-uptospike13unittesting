#include "StateManager.h"



AboutState* StateManager::fStateAbout = 0;
MenuState* StateManager::fStateMenu = 0;
HallOfFameState* StateManager::fStateHallOfFame = 0;
PlayingState* StateManager::fStatePlaying = 0;
SelectMapState* StateManager::fStateSelectMap = 0;
GameState* StateManager::fCurrentState = 0;
HelpState* StateManager::fStateHelp = 0;
bool StateManager::isRunning = true;

StateManager::StateManager()
{
	fStateAbout = new AboutState();
	fStateMenu = new MenuState();
	fStateHallOfFame = new HallOfFameState();
	fStateHelp = new HelpState();
	fStatePlaying = new PlayingState();
	fStateSelectMap = new SelectMapState();
	fCurrentState = fStateMenu;
}


StateManager::~StateManager()
{

}

void StateManager::changeState(State aState)
{
	switch (aState)
	{
		case MENU:	
			fCurrentState = fStateMenu;
			break;
		case HOF: 
			fCurrentState = fStateHallOfFame;
			break;
		case SELECTMAP:
			fCurrentState = fStateSelectMap;
			break;
		case ABOUT:
			fCurrentState = fStateAbout;
			break;
		case PLAYING:
			delete fStatePlaying;
			fStatePlaying = new PlayingState();
			fCurrentState = fStatePlaying;
			break;
		case HELP:
			fCurrentState = fStateHelp;
			break;
		case QUIT:
			fCurrentState = fStateMenu;
			isRunning = false;
			break;
		default:
			break;
	}
}

bool StateManager::getIsRunning()
{
	return isRunning;
}


void StateManager::HandleInput()
{
	fCurrentState->HandleInput();
}

GameState* StateManager::getCurrentState()
{
	return fCurrentState;
}


