#pragma once
#include "CharacteristicComponent.h"
class Flammability : public CharacteristicComponent
{
public:
	Flammability(int aValue);
	Flammability();
	~Flammability();

	//Methods
	string message(GameObject &aGameObject);
};

