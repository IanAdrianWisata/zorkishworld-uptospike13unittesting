#pragma once
#include "CharacteristicComponent.h"

class Destructible : public CharacteristicComponent
{
public:
	Destructible(int aValue);
	Destructible();
	~Destructible();

	//Methods
	string message(GameObject &aGameObject);
};

