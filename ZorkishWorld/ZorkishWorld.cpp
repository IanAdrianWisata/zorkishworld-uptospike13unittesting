#include "ZorkishWorld.h"
#include "windows.h"

ZorkishWorld::ZorkishWorld()
{
	//Setting Up The StateManager
	fStateManager = new StateManager();
	DrawWorld();
}


ZorkishWorld::~ZorkishWorld()
{
}

void ZorkishWorld::HandleInput()
{
	//Handles the Input
	if (isRunning())

		fStateManager->HandleInput();
}

void ZorkishWorld::DrawWorld()
{
	if (isRunning())
	{
		fStateManager->getCurrentState()->Draw();
	}
}

void ZorkishWorld::UpdateWorld()
{
	fStateManager->getCurrentState()->Update();
}

bool ZorkishWorld::isRunning()
{
	return (fStateManager->getIsRunning());
}
