#pragma once
#include "GameState.h"
#include "CommandManager.h"
#include "ItemContainer.h"
class PlayingState : public GameState
{
private:
	Player* fPlayer;
	vector<Location>* fLocations;
	vector<Path>* fPaths;
	CommandManager* fCommandManager;
	string fActionMessageResult;
public:
	//Constructors
	PlayingState();
	~PlayingState();

	//Methods
	void HandleInput();
	void Update();
	void Draw();

	void populateLocations();
	void populatePaths();
	void attachPathToLocations();
};

