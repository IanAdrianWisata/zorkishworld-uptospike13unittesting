#include "Inventory.h"



Inventory::Inventory()
{
	fItems = new vector<Item*>();
}


Inventory::~Inventory()
{
}

void Inventory::AddItem(Item& aItem)
{
	fItems->push_back(&aItem);
}

void Inventory::RemoveItem(string aItemName)
{
	
	for (int i = 0; i < fItems->size() ; i++)
	{
		if ((*fItems)[i]->getIdentifiers() == aItemName)
		{
			fItems->erase((fItems->begin() + i));
		}
	}
}

bool Inventory::HasItem(string & aItemName)
{
	bool result = false;
	for (int i = 0; i < fItems->size(); i++)
	{
		if ((*fItems)[i]->getIdentifiers() == aItemName)
		{
			result = true;
		}
	}
	return result;
}

Item* Inventory::FetchItem(string & aItemName)
{
	Item* result = NULL;
	for (int i = 0; i < fItems->size(); i++)
	{
		if ((*fItems)[i]->getIdentifiers() == aItemName)
		{
			result = (*fItems)[i];
			RemoveItem(aItemName);
		}
	}
	return result ;
}

string Inventory::CheckInventory()
{
	string result = "";
	for (int i = 0; i < fItems->size(); i++)
	{
		result = (*fItems)[i]->getName() + " " + (*fItems)[i]->getDescription() + ";\n";
	}
	return result;
}

string Inventory::getItemList()
{
	string result = "";
	for (int i = 0; i < fItems->size(); i++)
	{
		result += (*fItems)[i]->getName() + ", ";
	}

	if (result == "")
		result = "nothing";
	return result;
}

int Inventory::getItemSize()
{
	return fItems->size();
}

Item Inventory::getItem(string &aItemName)
{
	Item result;
	for (int i = 0; i < fItems->size(); i++)
	{
		if ((*fItems)[i]->getIdentifiers() == aItemName)
			result = *(*fItems)[i];
	}
	return result;
}

Item * Inventory::getItemptr(string & aItemName)
{
	Item* result = NULL;
	for (int i = 0; i < fItems->size(); i++)
	{
		if ((*fItems)[i]->getIdentifiers() == aItemName)
			result = (*fItems)[i];
	}
	return result;
}
