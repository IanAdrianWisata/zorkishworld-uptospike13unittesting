#pragma once
#include "GameState.h"
class SelectMapState : public GameState
{
public:
	//Constructors
	SelectMapState();
	~SelectMapState();

	//Methods
	void HandleInput();
	void Update();
	void Draw();

};

