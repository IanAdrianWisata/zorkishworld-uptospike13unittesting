#include "GameObject.h"
#include "Message.h"


GameObject::GameObject(string aIdentifiers, string aName, string aDescription) :IdentifiableObject(aIdentifiers)
{
	fIdentifiers = aIdentifiers;
	fName = aName;
	fDescription = aDescription;
	fCharacteritics = new vector<CharacteristicComponent*>;
	fMessageQueue = MessageQueue();
}

GameObject::GameObject()
{
}


GameObject::~GameObject()
{
}

void GameObject::addComponent(string aComponent)
{
	if (aComponent == "burn")
		fCharacteritics->push_back(new Flammability);
	else
		fCharacteritics->push_back(new Destructible);
}

void GameObject::removeComponent(string aComponent)
{
	for (int i = 0; i < fCharacteritics->size(); i++)
	{
		if ((*fCharacteritics)[i]->getType() == aComponent)
			fCharacteritics->erase(fCharacteritics->begin() + i);
	}
}

void GameObject::onMessage(Message aMsg)
{

}

string GameObject::getIdentifiers()
{
	return fIdentifiers;
}

string GameObject::getName()
{
	return fName;
}

string GameObject::getDescription()
{
	return fDescription;
}

MessageQueue GameObject::getMessageQueue()
{
	return fMessageQueue;
}

vector<CharacteristicComponent*> GameObject::getCharacteristics()
{
	return *fCharacteritics;
}
