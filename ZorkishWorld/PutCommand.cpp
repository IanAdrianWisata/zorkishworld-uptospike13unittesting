#include "PutCommand.h"



PutCommand::PutCommand() :Command("put")
{
}


PutCommand::~PutCommand()
{
}

string PutCommand::Execute(Player & aPlayer, string aText)
{
	
	vector<string> lInput = Utilities::splice(aText, ' ');
	Inventory* lPlayerInventory = aPlayer.getInventory();
	string result = "";
	
	if (lInput.size() == 1)
		return "What do you want to put? \n";
	if (lInput.size()  < 4)
		return "Where do you want to put " + lInput[1] + " to? " + "\n";
	if (lInput.size() == 4)
	{
		if (lInput[2] == "to" || lInput[2] == "into" || lInput[2] == "in")
		{
			//Check if player has both container and item
			if (aPlayer.getInventory()->HasItem(lInput[1]))
			{
				if (aPlayer.getInventory()->HasItem(lInput[3]) && lInput[1] != lInput[3])
				{
					Item* temp = (lPlayerInventory->getItemptr(lInput[3]));

					if (ItemContainer* lBag = dynamic_cast<ItemContainer*>(temp))
					{
						Item* lItem = lPlayerInventory->FetchItem(lInput[1]);
						lBag->getInventory().AddItem((*lItem));
						result = "You put " + lInput[1] + " in " + lInput[3] + "\n";

					}			
	
					else
						result = lInput[3] + " cannot contain an item! \n";
				}
				else
					result = "You don't have " + lInput[3] + " to store the " + lInput[1] + "\n";
			}
			else
				result = "You don't have " + lInput[1] + "\n";
		}
		else
			result = "Wrong Command! \n";
		
	}
	return result;
}
