#pragma once
#include "Command.h"
class TakeCommand : public Command
{
public:

	//Constructors
	TakeCommand();
	~TakeCommand();

	//Methods
	string Execute(Player &aPlayer, string aText);
};

