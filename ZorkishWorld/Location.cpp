#include "Location.h"

Location::Location(string aName, string aDescription)
{
	fPaths = new vector<Path>();
	fInventory = new Inventory();
	fName = aName;
	fDescription = aDescription;

	
	fMonsters.push_back(new Monster("slime", "Slime", "test monster", 20, 1));
	fMonsters.push_back(new Monster("troll", "Troll", "test monster2", 40, 2));
	
	
}

Location::Location()
{
}

void Location::AddDestination(Path &aPath)
{
	fPaths->push_back(aPath);
}

void Location::addMonster(Monster & aMonster)
{
	fMonsters.push_back(&aMonster);
}

bool Location::hasMonster(string & aMonsterName)
{
	bool result = false;
	for (int i = 0; i< fMonsters.size(); i++)
	{
		if (fMonsters[i]->getIdentifiers() == aMonsterName && fMonsters[i]->isAlive())
			result = true;
	}
	return result ;
}

void Location::Update()
{
	for (int i = 0; i< fMonsters.size(); i++)
	{
		fMonsters[i]->getMessageQueue().dispatchMessage();
	}
}

Monster* Location::getMonster(string & aMonsterName)
{
	for (int i = 0; i< fMonsters.size(); i++)
	{
		if (fMonsters[i]->getIdentifiers() == aMonsterName && fMonsters[i]->isAlive())
			return (fMonsters[i]);
	}
	return NULL;
}


Location::~Location()
{
}

string Location::getLocationName()
{
	return fName;
}

string Location::getLocationDescription()
{
	return fDescription;
}

string Location::getPathsList()
{
	string result = "";
	for (int i = 0; i< fPaths->size(); i++)
	{
		result += (*fPaths)[i].getId() + "; ";
	}
	return	result;
}

string Location::getMonsterList()
{
	string result = "";
	for (int i = 0; i< fMonsters.size(); i++)
	{
		if (fMonsters[i]->isAlive())
			result += fMonsters[i]->getName() + "; ";
	}
	if (result == "")
		result = "There are no monsters";
	return	result;
}

vector<Path> Location::getPaths()
{
	return *fPaths;
}

Inventory* Location::getInventory()
{
	return fInventory;
}

vector<Monster*> Location::getMonsters()
{
	return fMonsters;
}
