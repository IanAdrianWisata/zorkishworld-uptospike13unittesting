#pragma once
#include "Command.h"
class PutCommand : public Command
{
public:
	
	//Constructors
	PutCommand();
	~PutCommand();

	//Methods
	string Execute(Player &aPlayer, string aText);
};

