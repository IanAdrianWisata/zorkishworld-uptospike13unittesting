#pragma once
#include <vector>
#include "Message.h"
using namespace std;

class MessageQueue
{
private:
	vector<Message>* fMessages;
public:
	MessageQueue();
	~MessageQueue();

	//Methods
	void addMessage(Message &aMessage);
	void dispatchMessage();
};

