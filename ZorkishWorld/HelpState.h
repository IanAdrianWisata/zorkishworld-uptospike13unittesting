#pragma once
#include "GameState.h"
class HelpState : public GameState
{
public:

	//Constructors
	HelpState();
	~HelpState();

	//Methods
	void HandleInput();
	void Update();
	void Draw();
};

