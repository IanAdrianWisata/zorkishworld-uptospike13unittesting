#pragma once
#include <string>
#include "Location.h"
using namespace std;

class Location;

class Path
{
private:
	string fId;
	string fDescription;
	Location* fStartLocation;
	Location* fDestination;

public:

	//Constructors
	Path(string aId, string aDescription, Location* aStartLocation, Location* aLocation);
	Path();
	~Path();


	//Properties
	string getId();
	string getDescription();
	Location* getStartLocation();
	Location* getDestination();
};

