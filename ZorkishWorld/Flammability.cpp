#include "Flammability.h"
#include "GameObject.h"


Flammability::Flammability(int aValue) :CharacteristicComponent(aValue, (string)"burn")
{
}

Flammability::Flammability() : CharacteristicComponent((string)"burn")
{
}


Flammability::~Flammability()
{
}

string Flammability::message(GameObject &aGameObject)
{
	return "You burned " + aGameObject.getName() + "\n";
}
