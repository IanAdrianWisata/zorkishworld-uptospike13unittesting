#include "CommandManager.h"



CommandManager::CommandManager()
{
	addCommands(MoveCommand());
	addCommands(LookCommand());
	addCommands(PutCommand());
	addCommands(TakeCommand());
	addCommands(ActionCommand());
	addCommands(AttackCommand());
}


CommandManager::~CommandManager()
{
}

void CommandManager::addCommands(Command aCommand)
{
	fCommands.push_back(aCommand);
}

string CommandManager::processCommands(string aString, Player &aPlayer)
{
	vector<string> lInput = Utilities::splice(aString, ' ');
	MoveCommand moveCommand;
	LookCommand lookCommand;
	PutCommand putCommand;
	TakeCommand takeCommand;
	ActionCommand actionCommand;
	AttackCommand attackcommand;
	for (int i = 0; i < fCommands.size(); i++)
	{
		if (fCommands[i].isIdentifiable(lInput[0]))
		{
			switch (i)
			{
			case 0:
				return moveCommand.Execute(aPlayer, aString);
			case 1:
				return lookCommand.Execute(aPlayer, aString);
				break;
			case 2:
				return putCommand.Execute(aPlayer, aString);
			case 3:
				return takeCommand.Execute(aPlayer, aString);
			case 4:
				return actionCommand.Execute(aPlayer, aString);
				break;
			case 5:
				return attackcommand.Execute(aPlayer, aString);
				break;
			default:
				return "Wrong Command";
			}
		}
	}
	return "No Command Found \n";
}

vector<Command> CommandManager::getCommands()
{
	return fCommands;
}
