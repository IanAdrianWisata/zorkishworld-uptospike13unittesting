#pragma once
#include "IdentifiableObject.h"
#include "CharacteristicComponent.h"
#include "Flammability.h"
#include "Destructible.h"
#include "MessageQueue.h"

class MessageQueue;

class GameObject : public IdentifiableObject
{
private:
	string fIdentifiers;
	string fName;
	string fDescription;
	vector<CharacteristicComponent*>* fCharacteritics;
	MessageQueue fMessageQueue;
public:
	//Constructors
	GameObject(string aIdentifiers, string aName, string aDescription);
	GameObject();
	~GameObject();

	//Methods
	void addComponent(string aComponent);
	void removeComponent(string aComponent);
	virtual void onMessage(Message aMsg);


	//Properties
	string getIdentifiers();
	string getName();
	string getDescription();
	MessageQueue getMessageQueue();
	vector<CharacteristicComponent*> getCharacteristics();
};

