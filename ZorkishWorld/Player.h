#pragma once
#include "GameObject.h"
#include "Inventory.h"
#include "Location.h"
#include "ItemContainer.h"

class Player : public GameObject
{
private:
	int fHealthPoints;
	int fBaseDamage;
	Inventory* fInventory;
	Location* fLocation;
public:
	//Constructors
	Player(Location &aLocation);
	~Player();

	//Methods
	void onMessage(Message aMessage);
	void Move(Location& aLocation);
	void attack(GameObject &aTarget);
	//Properties
	Inventory* getInventory();
	Location getLocation();

	int getHealthPoints();
	int getDamage();
};

