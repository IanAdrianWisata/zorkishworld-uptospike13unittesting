#pragma once
#include "Command.h"
class ActionCommand : public Command 
{
public:
	ActionCommand();
	~ActionCommand();

	string Execute(Player &aPlayer, string aText);
};
