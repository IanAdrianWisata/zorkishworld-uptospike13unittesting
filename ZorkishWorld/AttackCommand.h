#pragma once
#include "Command.h"
class AttackCommand :public Command
{
private:

public:
	AttackCommand();
	~AttackCommand();

	//Methods
	string Execute(Player &aPlayer, string aText);
};

