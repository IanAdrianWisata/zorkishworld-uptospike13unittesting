#pragma once
#include "ZorkishWorld.h"
#include "Utilities.h"
#include "Location.h"
#include "CommandManager.h"
#include "MoveCommand.h"
#include "LookCommand.h"


int main()
{
	ZorkishWorld myWorld;
	while (myWorld.isRunning())
	{
		myWorld.HandleInput();
		myWorld.UpdateWorld();
		myWorld.DrawWorld();
	}
	return 0;
}