#pragma once
#include "GameState.h"
class NewHighScoreState : public GameState
{
public:

	//Constructors
	NewHighScoreState();
	~NewHighScoreState();

	//Methods
	void HandleInput();
	void Update();
	void Draw();
};

