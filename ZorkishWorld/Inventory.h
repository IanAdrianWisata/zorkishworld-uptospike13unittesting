#pragma once
#include "Item.h"
#include <vector>
#include <iostream>

using namespace std;

class Inventory
{
private:
	vector<Item*>* fItems;
public:
	//Constructors
	Inventory();
	~Inventory();

	//Methods
	void AddItem(Item& aItem);
	void RemoveItem(string aItemName);
	bool HasItem(string &aItemName);
	Item* FetchItem(string &aItemName);

	//Properties
	string CheckInventory();
	string getItemList();
	int getItemSize();
	Item getItem(string &aItemName);
	Item* getItemptr(string &aItemName);
};

