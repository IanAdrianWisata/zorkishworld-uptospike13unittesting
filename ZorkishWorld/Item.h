#pragma once
#include "GameObject.h"

class Item :public GameObject
{
private:
	string fContainer;
public:
	//Constructors
	Item(string aId, string aName, string aDescription, string aContainer); 
	Item();
	~Item();

	//Methods
	virtual bool isContainer();

	//Properties
	Item getItem();
	Item* getItemptr();
};