#pragma once
#include "GameState.h"

class AboutState : public GameState
{
public:
	//Constructors
	AboutState();
	~AboutState();

	//Methods
	void HandleInput();
	void Update();
	void Draw();
};

