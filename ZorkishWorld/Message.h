#pragma once
#include <string>

using namespace std;

class GameObject;

class Message
{
private:
	GameObject* fSender;
	GameObject* fDestination;
	string fType;
	int fData;
public:
	Message(GameObject &aSender, GameObject &aDestinnation, string aType, int aData);
	Message();
	~Message();

	//Methods
	GameObject* getDestination();
	string getType();
	int getData();
};

