#include "ActionCommand.h"



ActionCommand::ActionCommand() :Command("burn,destroy,break")
{
}


ActionCommand::~ActionCommand()
{
}

string ActionCommand::Execute(Player & aPlayer, string aText)
{
	vector<string> lInput = Utilities::splice(aText, ' ');
	string result = "";

	if (lInput.size() != 2)
		result += "Wrong Command! \n";
	else
	{
		if (aPlayer.getInventory()->HasItem(lInput[1]))
		{
			Item* temp = aPlayer.getInventory()->getItemptr(lInput[1]);

			for (int i = 0; i < temp->getCharacteristics().size(); i++)
			{
				if (temp->getCharacteristics()[i]->isIdentifiable(lInput[0]))
				{
					result = temp->getCharacteristics()[i]->message(*temp);
					aPlayer.getInventory()->RemoveItem(lInput[1]);
				}
				else
					result = lInput[1] + " cannot be " + lInput[0] + "\n";
			}
		}
		else
			result = "You don't have " + lInput[1] + " to " + lInput[0] + "\n";
	}



	return result;
}
