#include "AttackCommand.h"
#include "Message.h"

AttackCommand::AttackCommand() :Command((string)"attack,hit")
{
}


AttackCommand::~AttackCommand()
{
}

string AttackCommand::Execute(Player & aPlayer, string aText)
{
	vector<string> lInput = Utilities::splice(aText, ' ');
	string result = "";
	
	
	if (lInput.size() == 2)
	{
		if (lInput[1] == "monsters")
		{
			for (int i = 0; i < aPlayer.getLocation().getMonsters().size(); i++)
			{
				Monster* temp = (aPlayer.getLocation().getMonsters()[i]);
				aPlayer.attack(*temp);
				temp->attack(aPlayer);
			}
		}
		if (aPlayer.getLocation().hasMonster(lInput[1])) //if monster is alive and in location
		{
			if (aPlayer.getLocation().hasMonster(lInput[1])) //if monster is alive and in location
			{
				Monster* temp = (aPlayer.getLocation().getMonster(lInput[1]));
				aPlayer.attack(*temp);
				temp->attack(aPlayer);
			}
		}
	}
	else
	{
		result = "Wrong Command! \n";
	}
		
	return result;
}
