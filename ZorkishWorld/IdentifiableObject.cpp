#include "IdentifiableObject.h"



IdentifiableObject::IdentifiableObject()
{
	
}


IdentifiableObject::IdentifiableObject(string &aIds)
{
	fIdentifiers = Utilities::splice(aIds, ',');
}



IdentifiableObject::~IdentifiableObject()
{
}

bool IdentifiableObject::isIdentifiable(string aString)
{
	bool result = false;
	for (int i = 0; i < fIdentifiers.size(); i++)
	{
		if ((fIdentifiers)[i] == aString)
		{
			result = true;
		}
	}
	return result;
}

int IdentifiableObject::getNumberOfItem()
{
	return fIdentifiers.size();
}
