#pragma once
#include "GameState.h"
class MenuState : public GameState
{
private:

public:

	//Constructors
	MenuState();
	~MenuState();

	//Methods
	void HandleInput();
	void Update();
	void Draw();
};

