#pragma once
#include "IdentifiableObject.h"

class GameObject;

class CharacteristicComponent : public IdentifiableObject
{
private:
	int resistanceValue;
	string fType;
public:
	//Constructors
	CharacteristicComponent(int aValue, string aCType);
	CharacteristicComponent(string aCType);
	CharacteristicComponent();
	~CharacteristicComponent();

	//Methods
	virtual string message(GameObject &aGameObject);

	//Properties
	int getrestistanceValue();
	string getType();
};

