#pragma once
#include "GameState.h"
class HallOfFameState : public GameState
{
public:

	//Constructors
	HallOfFameState();
	~HallOfFameState();

	//Methods
	void HandleInput();
	void Update();
	void Draw();

};

