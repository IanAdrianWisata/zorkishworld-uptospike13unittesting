#pragma once
#include "IdentifiableObject.h"
#include "Player.h"
class Command : public IdentifiableObject
{
private:
public:
	Command(string aString);
	Command();
	~Command();

	//Methods
	virtual string Execute(Player &aPlayer, string aText);
	//Properties
	int getNumberOfItem();
};

