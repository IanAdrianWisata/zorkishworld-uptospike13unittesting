#pragma once
#include "Command.h"
#include "Location.h"
#include "Path.h"

class MoveCommand : public Command
{
public:

	//Constructors
	MoveCommand();
	~MoveCommand();

	//Methods
	string Execute(Player &aPlayer, string aText);
};

