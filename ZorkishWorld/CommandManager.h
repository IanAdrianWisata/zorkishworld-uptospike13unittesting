#pragma once
#include "Command.h"
#include "MoveCommand.h"
#include "LookCommand.h"
#include "PutCommand.h"
#include "TakeCommand.h"
#include "ActionCommand.h"
#include "AttackCommand.h"
class CommandManager
{
private:
	vector<Command> fCommands;
public:

	//Constructors
	CommandManager();
	~CommandManager();

	//Methods
	void addCommands(Command aCommand);
	string processCommands(string aString, Player &aPlayer);
	
	//Properties
	vector<Command> getCommands();
};

