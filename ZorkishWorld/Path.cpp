#include "Path.h"




Path::Path(string aId, string aDescription, Location* aStartLocation, Location* aDestination)
{
	fId = aId;
	fDescription = aDescription;
	fStartLocation = aStartLocation;
	fDestination = aDestination;
}

Path::Path()
{
}


Path::~Path()
{
}

string Path::getId()
{
	return fId;
}

string Path::getDescription()
{
	return fDescription;
}

Location* Path::getStartLocation()
{
	return fStartLocation;
}

Location* Path::getDestination()
{
	return fDestination;
}
