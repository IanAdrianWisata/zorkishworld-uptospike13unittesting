#pragma once
#include "GameObject.h"
#include <sstream>
class Monster : public GameObject
{
private:
	bool fAlive;
	int fHealthPoints;
	int fDamage;
public:
	//Constructors
	Monster(string aId, string aName, string aDescription,int aHealthPoints,int aDamage);
	Monster();
	~Monster();

	//Methods
	void onMessage(Message aMessage);
	void Update();
	void attack(GameObject &aTarget);

	//Properties
	bool isAlive();
	int getHealthPoints();
	int getDamage();
	string getStatus();
};

