#pragma once
#include "StateManager.h"
class ZorkishWorld
{
private:
	StateManager* fStateManager;

public:
	//Constructors
	ZorkishWorld();
	~ZorkishWorld();
	
	//Methods
	void HandleInput();
	void DrawWorld();
	void UpdateWorld();
	
	bool isRunning();
	//Properties
};

