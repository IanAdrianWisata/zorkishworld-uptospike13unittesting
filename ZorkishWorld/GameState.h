#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Utilities.h"

using namespace std;


class GameState
{
public:
	//Constructors
	GameState();
	GameState(GameState &aState, string aName);
	~GameState();

	//Methods
	virtual void HandleInput();
	virtual void Update();
	virtual void Draw();

	//Properties
};

