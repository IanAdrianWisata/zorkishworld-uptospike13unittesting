#pragma once
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
using namespace std;


class Utilities
{
private:
	//Locations
	static vector<string> fLocations;
	//Paths
	static vector<string> fPaths;
	//Items
	static vector<string> fItems;
	//Map
	static string fMapName;

public:

	//Constructors
	Utilities();
	~Utilities();

	//Methods
	static vector<string> splice(string aString, char aDelimiter);
	static void readInput(string aLocationsFile, string aFilePaths);
	static void readLocationsFromTextFile(ifstream& aInput);
	static void readPathsFromTextFile(ifstream& aInput);
	static void readItemsFromTextFile(ifstream& aInput);
	static void loadMap(string aMapName);
	static void eraseData();

	//Properties
	static vector<string>getItems();
	static vector<string> getLocations();
	static vector<string> getPaths();
	static string getMapName();

};

