#include "MenuState.h"
#include "StateManager.h"


MenuState::MenuState() 
{
}


MenuState::~MenuState()
{
}

void MenuState::HandleInput()
{
	int lInput;
	cin >> lInput;

	switch (lInput)
	{
	case 1:
		 StateManager::changeState(StateManager::SELECTMAP);
		 break;
	case 2:
		StateManager::changeState(StateManager::HOF);
		break;
	case 3:
		StateManager::changeState(StateManager::HELP);
		break;
	case 4:
		StateManager::changeState(StateManager::ABOUT);
		break;
	case 5:
		StateManager::changeState(StateManager::QUIT);
		break;
	default:
		cout << "Wrong Input!";
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		
	}
}
void MenuState::Update()
{

}
void MenuState::Draw()
{
	system("cls");
	cout << "Zorkish :: Main Menu \n";
	cout << "------------------------------------------------------------------------------ \n";
	cout << "Welcome to Zorkish Adventures \n";
	cout << "1. Select Adventure and Play \n";
	cout << "2. Hall of Fame \n";
	cout << "3. Help \n";
	cout << "4. About \n";
	cout << "5. Quit \n";
}


