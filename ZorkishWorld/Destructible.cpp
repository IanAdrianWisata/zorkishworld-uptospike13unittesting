#include "Destructible.h"
#include "GameObject.h"


Destructible::Destructible(int aValue) :CharacteristicComponent(aValue, (string)"hit,destroy,break")
{
}

Destructible::Destructible() : CharacteristicComponent((string)"hit,destroy,break")
{
}


Destructible::~Destructible()
{
}

string Destructible::message(GameObject &aGameObject)
{
	return "You destroyed " + aGameObject.getName() + "\n";
}