#pragma once
#include "Inventory.h"
#include "Item.h"

class ItemContainer : public Item
{
private:
	Inventory* fInventory;
	string fContainer;
public:
	//Constructors
	ItemContainer(string aId, string aName, string aDescription, string aContainer);
	ItemContainer();
	~ItemContainer();

	//Methods
	Inventory getInventory();
	bool isContainer();
};

